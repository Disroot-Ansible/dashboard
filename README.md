# Dashboard - Ansible Role

This role covers deployment and configuration of Dashboard. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.

`vagrant up`

`ansible-playbook -b Playbooks/dashboard.yml`

Then you can access dashboard from your computer on http://192.168.33.49


## Playbook
The playbook includes nginx role and deploys entire stack needed to run Dashboard. Additional role is also available in the Ansible roles repos in git.


## CHANGELOG
- **14.04.2021** - Vagrant deployment
   - Make the role deployadable on vagrant
   - Update readme
   - Add licence
